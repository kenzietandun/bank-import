#!/usr/bin/env python3

import base64
from io import BytesIO
from flask import Flask, g, request, render_template, redirect
import psycopg2
from psycopg2.extras import RealDictCursor
from flask_cors import CORS
from flask_mail import Mail, Message
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import os

app = Flask(__name__)
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = os.getenv('MAIL_USERNAME')
app.config['MAIL_PASSWORD'] = os.getenv('MAIL_PASSWORD')
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True

mail = Mail(app)
CORS(app)

PG_USERNAME = os.getenv('PG_USERNAME')
PG_PASSWORD = os.getenv('PG_PASSWORD')


@app.before_request
def setup_conn():
    g.conn = psycopg2.connect(
        user=PG_USERNAME,
        password=PG_PASSWORD,
        host="127.0.0.1",
        port="5432",
        database="bank",
        cursor_factory=RealDictCursor,
    )


def _get_persons():
    cursor = g.conn.cursor()
    cursor.execute("""
        SELECT id, name
        FROM person
    """)
    return cursor.fetchall()


def _get_transaction_entries():
    cursor = g.conn.cursor()
    cursor.execute("""
        SELECT
            id,
            entry_date,
            day_from_dow_index(EXTRACT(dow FROM entry_date)) AS day_of_week,
            amount,
            payee,
            amount < 0::MONEY AS is_owing,
            po.person_id AS debtor_id,
            po.is_paid AS paid
        FROM transaction
            LEFT JOIN person_owing po ON po.transaction_id = transaction.id
        WHERE payee NOT IN ('K TANDUN', 'OnCall Account')
            AND entry_date >= now() - '1 month'::INTERVAL
        ORDER BY id DESC;
    """)
    return cursor.fetchall()


@app.route("/transactions")
def get_transactions():
    return render_template('transactions.html',
                           entries=_get_transaction_entries(),
                           persons=_get_persons())


def _get_payments():
    cursor = g.conn.cursor()
    cursor.execute("""
        SELECT
            id,
            entry_date,
            day_from_dow_index(EXTRACT(dow FROM entry_date)) AS day_of_week,
            amount,
            payee
        FROM transaction
            LEFT JOIN person_owing po ON po.transaction_id = transaction.id
        WHERE transaction.id NOT IN (
                SELECT payment_transaction_id FROM person_owing WHERE payment_transaction_id IS NOT NULL
            )
            AND amount > '0'::MONEY
            AND payee NOT IN ('K TANDUN', 'OnCall Account')
            AND entry_date >= now() - '2 week'::INTERVAL
        ORDER BY entry_date DESC, id DESC;
    """)
    return cursor.fetchall()


@app.route("/transactions/payments")
def get_payments():
    payments = _get_payments()
    return render_template("payments.html", payments=payments)


def _get_debtors():
    cursor = g.conn.cursor()
    cursor.execute("""
        SELECT
            p.id AS person_id,
            p.name,
            SUM(t.amount)/2 AS owing_amount,
            ARRAY_AGG(t.id) AS transaction_ids,
            BOOL_AND(is_emailed) AS emailed
        FROM person p 
            LEFT JOIN person_owing po ON p.id = po.person_id
            LEFT JOIN transaction t ON t.id = po.transaction_id
        WHERE NOT po.is_paid
        GROUP BY p.id, p.name
    """)
    return cursor.fetchall()


@app.route("/debtors")
def get_debtors():
    return render_template('debtors.html', debtors=_get_debtors())


def _add_debtor(request):
    cursor = g.conn.cursor()
    cursor.execute(
        """
        INSERT INTO person (name, email_address)
        SELECT %(name)s, %(email)s
        WHERE NOT EXISTS (
            SELECT 1 FROM person WHERE name = %(name)s AND email_address = %(email)s
        )
    """, request)
    g.conn.commit()


@app.route("/debtors", methods=['POST'])
def add_debtor():
    _add_debtor(request.form.to_dict())
    return ''


def _add_owing(request):
    cursor = g.conn.cursor()
    cursor.execute(
        """
        INSERT INTO person_owing (transaction_id, person_id)
            SELECT %(transaction_id)s, %(person_id)s
            WHERE NOT EXISTS (
                SELECT 1 FROM person_owing 
                WHERE transaction_id = %(transaction_id)s AND person_id = %(person_id)s
            )
    """, request)
    g.conn.commit()


def _get_owing_transactions(received_payment):
    cursor = g.conn.cursor()
    cursor.execute(
        """
        SELECT 
            person.id AS person_id,
            person.name AS person_name,
            transaction_id,
            (transaction.amount / 2)::MONEY AS amount
        FROM person_owing 
        INNER JOIN transaction ON person_owing.transaction_id = transaction.id
        INNER JOIN person ON person.id = person_owing.person_id
        WHERE NOT is_paid
    """, (received_payment, ))
    return cursor.fetchall()


@app.route("/owing", methods=['GET'])
def get_owing_transactions():
    received_payment = request.args.get('amount')
    payment_transaction_id = request.args.get('payment_transaction_id')
    owings = _get_owing_transactions(received_payment) or []
    return render_template("payer_selection.html",
                           owings=owings,
                           payment_transaction_id=payment_transaction_id)


@app.route("/owing", methods=['POST'])
def add_owing():
    _add_owing(request.form.to_dict())
    return 'Awaiting payment'


def _send_email(transaction_ids, person_id, email_tone):
    cursor = g.conn.cursor()
    params = {
        'transaction_ids': [int(id) for id in transaction_ids],
        'person_id': person_id
    }
    cursor.execute(
        """
        SELECT
            (SELECT name FROM person WHERE id = %(person_id)s) AS name,
            (SELECT email_address FROM person WHERE id = %(person_id)s) AS email,
            (SELECT ABS(SUM(amount::NUMERIC) / 2)::MONEY FROM transaction
                WHERE id = ANY(%(transaction_ids)s)) AS total_amount,
            (SELECT JSON_AGG(JSON_BUILD_OBJECT('date', entry_date, 'payee', payee, 'owing_amount', amount / 2))
                FROM transaction
                WHERE id = ANY(%(transaction_ids)s)) AS transactions
    """, params)
    recipient_data = cursor.fetchall()
    recipient_data = recipient_data[0] if recipient_data else None
    if not recipient_data:
        raise Exception('Missing recipient data')

    email_tmpl = render_template(f"email_{email_tone}.html",
                                 data=recipient_data)

    msg = Message(f"{recipient_data.get('total_amount')} Debt Collection",
                  sender="nudnateiznek@gmail.com",
                  recipients=[recipient_data.get('email')])
    msg.html = email_tmpl
    mail.send(msg)

    cursor.execute(
        """
        UPDATE person_owing SET is_emailed = TRUE
        WHERE person_id = %(person_id)s
    """, params)
    g.conn.commit()


@app.route("/email/send", methods=['GET'])
def send_email():
    email_tone = request.args.get('tone', 'happy')
    person_id = request.args.get('person_id')
    transaction_ids = request.args.getlist('transaction_ids')
    _send_email(transaction_ids, person_id, email_tone)
    return 'Sent'


def _generate_plot(title, x_axis_label, y_axis_label, data_point):
    fig = Figure()
    fig, ax = plt.subplots(1, 1, tight_layout=True)
    ax.set_title(title)
    ax.set_xlabel(x_axis_label)
    ax.set_ylabel(y_axis_label)
    ax.bar(
        [p.get('x') for p in data_point.get('points')],
        [p.get('y') for p in data_point.get('points')],
    )
    buf = BytesIO()
    fig.savefig(buf, format='png')
    return base64.b64encode(buf.getbuffer()).decode('ascii')


def _get_spending_last_interval():
    cursor = g.conn.cursor()
    cursor.execute("""
        SELECT
        (
            SELECT  SUM(amount)
            FROM    transaction
            WHERE   entry_date >= now() - '1 week'::INTERVAL
                    AND amount < '0'::MONEY
        ) AS last_1_week,
        (
            SELECT  SUM(amount)
            FROM    transaction
            WHERE   entry_date >= now() - '2 week'::INTERVAL
                    AND amount < '0'::MONEY
        ) AS last_2_week,
        (
            SELECT  SUM(amount)
            FROM    transaction
            WHERE   entry_date >= now() - '1 month'::INTERVAL
                    AND amount < '0'::MONEY
        ) AS last_1_month,
        (
        SELECT  SUM(amount)
        FROM    transaction
        WHERE   entry_date >= now() - '2 month'::INTERVAL
                AND entry_date <= now() - '1 month'::INTERVAL
                AND amount < '0'::MONEY
        ) AS last_2_month
    """)
    spending = cursor.fetchall()
    return spending


def _get_summary():
    cursor = g.conn.cursor()
    cursor.execute("""
        WITH weekly_summary AS (
          SELECT (CASE
              WHEN date_part('week', entry_date) <= 52 THEN date_part('year', entry_date)
              ELSE date_part('year', entry_date) - 1
            END)::INTEGER AS year,
            (date_part('week', entry_date))::INTEGER AS week,
            ABS(SUM(amount::NUMERIC)) AS sum
          FROM transaction
          WHERE amount::NUMERIC < 0
          GROUP BY year, week
          ORDER BY year DESC, week DESC) 
        SELECT
          year,
          JSON_AGG(JSON_BUILD_OBJECT('x', week, 'y', sum)) AS points
        FROM weekly_summary
        GROUP BY year
        HAVING year >= 2021
        ORDER BY year DESC
    """)
    data_points = cursor.fetchall()
    return [
        _generate_plot(data_point.get('year'), 'Week Number',
                       'Total Spending ($)', data_point)
        for data_point in data_points
    ]


@app.route("/summary")
def get_summary():
    weekly_plots = _get_summary()
    return render_template('summary.html',
                           weekly_plots=weekly_plots,
                           spending=_get_spending_last_interval())


@app.route("/payments/assign", methods=['POST'])
def assign_payments():
    cursor = g.conn.cursor()
    cursor.execute(
        """
        UPDATE person_owing
        SET is_paid = TRUE,
            payment_transaction_id = %(payment_transaction_id)s
        WHERE transaction_id = %(transaction_id)s
            AND person_id = %(person_id)s
    """, request.form.to_dict())
    g.conn.commit()
    return 'Payment Assigned'


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ['csv']


@app.route("/upload", methods=['GET', 'POST'])
def upload_statement_csv():
    if request.method == 'GET':
        return render_template('upload.html')
    else:
        if 'file' not in request.files:
            return redirect(request.url)

        file = request.files.get('file')
        assert file is not None
        if file.filename == '':
            return redirect(request.url)

        assert file.filename is not None

        if file and allowed_file(file.filename):
            file.save('/tmp/upload.csv')

            cursor = g.conn.cursor()
            cursor.execute('''
                SET datestyle to DMY;
                COPY transaction (
                  entry_date ,
                  amount ,
                  payee ,
                  particulars ,
                  reference,
                  code ,
                  tran_type ,
                  from_account ,
                  to_account ,
                  serial ,
                  transaction_code ,
                  batch_number ,
                  originating_bank ,
                  processed_date 
                )
                FROM '/tmp/upload.csv' DELIMITER ',' CSV HEADER;
            ''')
        g.conn.commit()
        return 'Upload succeeded'
