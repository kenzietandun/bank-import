CREATE TABLE entry (
  entry_date DATE,
  amount MONEY,
  payee TEXT,
  particulars TEXT,
  reference TEXT,
  code TEXT,
  tran_type TEXT,
  from_account TEXT,
  to_account TEXT,
  serial TEXT,
  transaction_code TEXT,
  batch_number TEXT,
  originating_bank TEXT,
  processed_date DATE
);

ALTER TABLE entry ADD COLUMN id SERIAL PRIMARY KEY;
ALTER TABLE entry RENAME TO transaction;

CREATE TABLE person (
	id SERIAL PRIMARY KEY,
	name TEXT
);

CREATE TABLE person_owing (
	person_id INTEGER REFERENCES person(id),
	transaction_id INTEGER REFERENCES transaction(id)
);

ALTER TABLE person_owing ADD COLUMN is_paid BOOLEAN DEFAULT FALSE;
ALTER TABLE person_owing ADD COLUMN is_emailed BOOLEAN DEFAULT FALSE;

DROP FUNCTION IF EXISTS day_from_dow_index(NUMERIC);
CREATE OR REPLACE FUNCTION day_from_dow_index(
	_day_index NUMERIC
) RETURNS TEXT AS $$
DECLARE
	day TEXT;
BEGIN
	SELECT CASE 
		WHEN _day_index = 0 THEN 'Sunday'
		WHEN _day_index = 1 THEN 'Monday'
		WHEN _day_index = 2 THEN 'Tuesday'
		WHEN _day_index = 3 THEN 'Wednesday'
		WHEN _day_index = 4 THEN 'Thursday'
		WHEN _day_index = 5 THEN 'Friday'
		WHEN _day_index = 6 THEN 'Saturday'
	END INTO day;
	RETURN day;
END;
$$ LANGUAGE 'plpgsql';

ALTER TABLE person ADD COLUMN email_address TEXT NOT NULL DEFAULT 'me@example.com';

ALTER TABLE person_owing ADD COLUMN payment_transaction_id INTEGER REFERENCES transaction(id);
